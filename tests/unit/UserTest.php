<?php

class UserTest extends \PHPUnit_Framework_TestCase
{
    protected $user;

    public function setUp()
    {
        $this->user = new \App\Models\User;
    }

    /** @test */
    public function that_we_can_get_the_first_name()
    {
        $this->user->setFirstName('Vitalii');

        $this->assertEquals($this->user->getFirstName(), 'Vitalii');
    }

    public function testThatWeCanGetTheLastName()
    {
        $this->user->setLastName('Trofymenko');

        $this->assertEquals($this->user->getLastName(), 'Trofymenko');
    }

    public function testFullNameIsReturned()
    {
        $user = new \App\Models\User;
        $user->setFirstName('Vitalii');
        $user->setLastName('Trofymenko');

        $this->assertEquals($user->getFullName(), 'Vitalii Trofymenko');
    }

    public function testFirstAndLastNameAreTrimmed()
    {
        $user = new \App\Models\User;
        $user->setFirstName('Vitalii    ');
        $user->setLastName('       Trofymenko');

        $this->assertEquals($user->getFirstName(), 'Vitalii');
        $this->assertEquals($user->getLastName(), 'Trofymenko');
    }

    public function testEmailAddressCanBeSet()
    {
        $user = new \App\Models\User;
        $user->setEmail('vitalii@test.com');

        $this->assertEquals($user->getEmail(), 'vitalii@test.com');
    }

    public function testEmailVariablesContainCorrectValues()
    {
        $user = new \App\Models\User;
        $user->setFirstName('Vitalii');
        $user->setLastName('Trofymenko');
        $user->setEmail('vitalii@test.com');

        $emailVariables = $user->getEmailVariables();

        $this->assertArrayHasKey('full_name', $emailVariables);
        $this->assertArrayHasKey('email', $emailVariables);

        $this->assertEquals($emailVariables['full_name'], 'Vitalii Trofymenko');
        $this->assertEquals($emailVariables['email'], 'vitalii@test.com');
    }
}
